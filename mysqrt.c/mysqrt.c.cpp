// mysqrt.c.cpp : Defines the entry point for the console application.
//

#include <stdafx.h>

#include <stdio.h>
#include <math.h>

int main()
{
	double x = 2;
	double s_org=sqrt(x);

	int maxiter = 100;
	double tol = 1e-14;

	double s = 0.1;
	int i;
	for (i = 0; i < maxiter; i++)
	{
		printf("Befoe iteration %d, s=%20.15f\n", i, s);
		double s0 = s;
		s = 0.5*(s + x / s);

		double delta_s = s - s0;
		if (fabs(delta_s / x) < tol)
			break;

	}
	printf("After iteration\n");

	printf("sqrt(%f) = %20.15f\n", x, s_org);
	printf("mysqrt(%f) = %20.15f\n", x, s);
}
